function Problem1(inventory, passId) {
  let arr = []
  if (inventory == undefined || inventory.constructor != Array || inventory.length == 0) {
    return arr
  }
  else {
    for (let i = 0; i < inventory.length; i++) {
      if (inventory[i].id == passId) {
        return inventory[i];
      }
    }
    return arr
  }
}
module.exports = Problem1