function problem5(inventory) {
  let count = 0;
  let arr = [];
  if (inventory == undefined || inventory.constructor != Array || inventory.length == 0) {
      return arr
  }
  else {
      for (let i = 0; i < inventory.length; i++) {
          if (inventory[i].car_year < 2000) {
              count++;
              arr.push(inventory[i]);
          }
      } 
      console.log(arr);
  }
}
module.exports = problem5;
