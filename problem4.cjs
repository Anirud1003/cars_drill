function Problem4(inventory) {
  let arr = [];
  if (inventory == undefined || inventory.constructor != Array || inventory.length == 0) {
    return arr
  }
  for (let i = 0; i < inventory.length; i++) {
    arr.push(inventory[i].car_year);
  }
  return arr;
}
module.exports = Problem4;
