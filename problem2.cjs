function Problem2(inventory) {
  let arr = []
  if (inventory == undefined || inventory.constructor != Array || inventory.length == 0) {
      return arr
  }
  else {
      for (let i = 0; i < inventory.length; i++) {
          if (inventory[i].id === inventory.length) {
              return inventory[i];
          }
      }
  }
}

module.exports = Problem2
